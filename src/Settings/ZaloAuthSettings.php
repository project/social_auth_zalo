<?php

namespace Drupal\social_auth_zalo\Settings;

use Drupal\social_api\Settings\SettingsBase;

/**
 * Defines methods to get Social Auth Zalo app settings.
 */
class ZaloAuthSettings extends SettingsBase implements ZaloAuthSettingsInterface {

  /**
   * Application ID.
   *
   * @var string
   */
  protected $appId;

  /**
   * Application secret.
   *
   * @var string
   */
  protected $appSecret;

  /**
   * The default sdk version.
   *
   * @var string
   */
  protected $sdkVersion;

  /**
   * The callback url.
   *
   * @var string
   */
  protected $callbackUrl;
  /**
   * The default access token.
   *
   * @var string
   */
  protected $defaultToken;

  /**
   * {@inheritdoc}
   */
  public function getAppId() {
    if (!$this->appId) {
      $this->appId = $this->config->get('app_id');
    }
    return $this->appId;
  }

  /**
   * {@inheritdoc}
   */
  public function getAppSecret() {
    if (!$this->appSecret) {
      $this->appSecret = $this->config->get('app_secret');
    }
    return $this->appSecret;
  }

  /**
   * {@inheritdoc}
   */
  public function getSdkVersion() {
    if (!$this->sdkVersion) {
      $this->sdkVersion = $this->config->get('sdk_version');
    }
    return $this->sdkVersion;
  }

}
