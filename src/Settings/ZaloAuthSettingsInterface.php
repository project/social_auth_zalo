<?php

namespace Drupal\social_auth_zalo\Settings;

/**
 * Defines the settings interface.
 */
interface ZaloAuthSettingsInterface {

  /**
   * Gets the application ID.
   *
   * @return mixed
   *   The application ID.
   */
  public function getAppId();

  /**
   * Gets the application secret.
   *
   * @return string
   *   The application secret.
   */
  public function getAppSecret();

  /**
   * Gets the sdk version.
   *
   * @return string
   *   The version.
   */
  public function getSdkVersion();

}
