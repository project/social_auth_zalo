<?php

namespace Drupal\social_auth_zalo\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Routing\RequestContext;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\social_auth\Form\SocialAuthSettingsForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for Social Auth Zalo.
 */
class ZaloAuthSettingsForm extends SocialAuthSettingsForm {

  /**
   * The request context.
   *
   * @var \Drupal\Core\Routing\RequestContext
   */
  protected $requestContext;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   Used to check if route exists.
   * @param \Drupal\Core\Routing\RequestContext $request_context
   *   Holds information about the current request.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RouteProviderInterface $route_provider, RequestContext $request_context) {
    parent::__construct($config_factory, $route_provider);
    $this->requestContext = $request_context;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this class.
    return new static(
    // Load the services required to construct this class.
      $container->get('config.factory'),
      $container->get('router.route_provider'),
      $container->get('router.request_context')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_auth_zalo_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return array_merge(
      parent::getEditableConfigNames(),
      ['social_auth_zalo.settings']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('social_auth_zalo.settings');

    $form['zalo_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Zalo App settings'),
      '#open' => TRUE,
      '#description' => $this->t('You need to first create a Zalo App at <a href="@zalo-dev">@zalo-dev</a>', ['@zalo-dev' => 'https://developers.zalo.me']),
    ];

    $form['zalo_settings']['app_id'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Application ID'),
      '#default_value' => $config->get('app_id'),
      '#description' => $this->t('Copy the App ID of your Zalo App here. This value can be found from your App Dashboard.'),
    ];

    $form['zalo_settings']['app_secret'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('App Secret'),
      '#default_value' => $config->get('app_secret'),
      '#description' => $this->t('Copy the App Secret of your Zalo App here. This value can be found from your App Dashboard.'),
    ];

    $form['zalo_settings']['sdk_version'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Zalo SDK API version'),
      '#default_value' => $config->get('sdk_version'),
      '#description' => $this->t('Copy the API Version of your Zalo App here. This value can be found from your App Dashboard. More information on API versions can be found at <a href="@zalo-document">Zalo Developers Document</a>.', ['@zalo-document' => 'https://developers.zalo.me/docs']),
    ];
    $form['zalo_settings']['callback_url'] = [
      '#type' => 'textfield',
      '#disabled' => TRUE,
      '#title' => $this->t('Callback URL'),
      '#description' => $this->t('Copy this value to <em>Callback URL</em> field of your Zalo App settings.'),
      '#default_value' => $GLOBALS['base_url'] . '/user/login/zalo/callback',
    ];

    $form['zalo_settings']['app_domains'] = [
      '#type' => 'textfield',
      '#disabled' => TRUE,
      '#title' => $this->t('App Domains'),
      '#description' => $this->t('Copy this value to <em>App Domains</em> field of your Zalo App settings.'),
      '#default_value' => $this->requestContext->getHost(),
    ];

    $form['zalo_settings']['home_url'] = [
      '#type' => 'textfield',
      '#disabled' => TRUE,
      '#title' => $this->t('Home URL'),
      '#description' => $this->t('Copy this value to <em>Home URL</em> field of your Zalo App settings.'),
      '#default_value' => $GLOBALS['base_url'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!preg_match('/^[2-9]\.[0-9]{1,2}$/', $form_state->getValue('sdk_version'))) {
      $form_state->setErrorByName('sdk_version', $this->t('Invalid API version. The syntax for API version is for example <em>2.0</em>'));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('social_auth_zalo.settings')
      ->set('app_id', $values['app_id'])
      ->set('app_secret', $values['app_secret'])
      ->set('sdk_version', $values['sdk_version'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
