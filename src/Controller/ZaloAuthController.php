<?php

namespace Drupal\social_auth_zalo\Controller;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_api\User\UserManagerInterface;
use Drupal\social_auth\User\UserAuthenticator;
use Drupal\social_auth_zalo\ZaloAuthManager;
use Drupal\social_auth_zalo\ZaloAuthPersistentDataHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller for Social Auth Zalo.
 */
class ZaloAuthController extends ControllerBase {

  /**
   * The network plugin manager.
   *
   * @var \Drupal\social_api\Plugin\NetworkManager
   */
  private $networkManager;

  /**
   * The user manager.
   *
   * @var \Drupal\social_api\User\UserManagerInterface
   */
  private $userManager;

  /**
   * The user authenticator.
   *
   * @var \Drupal\social_auth\User\UserAuthenticator
   */
  private $userAuthenticator;

  /**
   * Used to access GET parameters.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $request;

  /**
   * The Zalo Persistent Data Handler.
   *
   * @var \Drupal\social_auth_zalo\ZaloAuthPersistentDataHandler
   */
  private $persistentDataHandler;

  /**
   * The Zalo authentication manager.
   *
   * @var \Drupal\social_auth_zalo\ZaloAuthManager
   */
  private $zaloManager;

  /**
   * MessengerInterface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * ZaloAuthController constructor.
   *
   * @param \Drupal\social_api\Plugin\NetworkManager $network_manager
   *   Used to get an instance of social_auth_zalo network plugin.
   * @param \Drupal\social_api\User\UserManagerInterface $user_manager
   *   Manages user login/registration.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   Used to access GET parameters.
   * @param \Drupal\social_auth_zalo\ZaloAuthPersistentDataHandler $persistent_data_handler
   *   ZaloAuthPersistentDataHandler.
   * @param \Drupal\social_auth_zalo\ZaloAuthManager $zalo_manager
   *   ZaloAuthManager.
   * @param \Drupal\social_auth\User\UserAuthenticator $user_authenticator
   *   UserAuthenticator.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   MessengerInterface.
   */
  public function __construct(NetworkManager $network_manager,
                              UserManagerInterface $user_manager,
                              RequestStack $request,
                              ZaloAuthPersistentDataHandler $persistent_data_handler,
                              ZaloAuthManager $zalo_manager,
                              UserAuthenticator $user_authenticator,
                              MessengerInterface $messenger) {
    $this->networkManager = $network_manager;
    $this->userManager = $user_manager;
    $this->request = $request;
    $this->persistentDataHandler = $persistent_data_handler;

    // Sets the plugin id.
    $this->userManager->setPluginId('social_auth_zalo');
    $this->userAuthenticator = $user_authenticator;
    $this->zaloManager = $zalo_manager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.network.manager'),
      $container->get('social_auth.user_manager'),
      $container->get('request_stack'),
      $container->get('social_auth_zalo.persistent_data_handler'),
      $container->get('social_auth_zalo.manager'),
      $container->get('social_auth.user_authenticator'),
      $container->get('messenger')
    );
  }

  /**
   * Redirect to Zalo login url.
   */
  public function redirectToZaloLogin() {
    /* @var \Zalo\Zalo|false $zalo */
    try {
      $zalo = $this->networkManager->createInstance('social_auth_zalo')
        ->getSdk();
    }
    catch (PluginException $e) {
      $this->messenger->addError($this->t('Cannot create instance of plugin social_auth_zalo.'));
      return $this->redirect('user.login');
    }

    // If zalo client could not be obtained.
    if (!$zalo) {
      $this->messenger->addError($this->t('Social Auth Zalo not configured properly. Contact site administrator.'));
      return $this->redirect('user.login');
    }

    $this->zaloManager->setClient($zalo);
    $zalo_login_url = $this->zaloManager->getZaloLoginUrl();

    return new TrustedRedirectResponse($zalo_login_url);
  }

  /**
   * Response for path 'user/login/zalo/callback'.
   */
  public function returnFromZalo() {
    // Get return code from url parameters.
    $code = $this->request->getCurrentRequest()->get('code');

    /* The document of Zalo dont describe a bout error case.
     * https://developers.zalo.me/docs/sdk/php-sdk/tai-lieu/social-api-post-1444
     * Just check the code parameter like their sample.
     */
    if (empty($code)) {
      $this->messenger->addError($this->t('You could not be authenticated.'));
      return $this->redirect('user.login');
    }

    /* @var \Zalo\Zalo|false $zalo */
    try {
      $zalo = $this->networkManager->createInstance('social_auth_zalo')
        ->getSdk();
    }
    catch (PluginException $e) {
      $this->messenger->addError($this->t('Cannot create instance of plugin social_auth_zalo.'));
      return $this->redirect('user.login');
    }

    // If zalo client could not be obtained.
    if (!$zalo) {
      $this->messenger->addError($this->t('Social Auth Zalo not configured properly. Contact site administrator.'));
      return $this->redirect('user.login');
    }

    $this->zaloManager->setClient($zalo)->authenticate();

    if (!$zalo_profile = $this->zaloManager->getUserInfo()) {
      $this->messenger->addError($this->t('Zalo login failed, could not load Zalo profile. Contact site administrator.'));
      return $this->redirect('user.login');
    }
    // Gets user's id from the Zalo profile.
    if (!$id = $this->zaloManager->getId($zalo_profile)) {
      $this->messenger->addError($this->t('Zalo login failed. This site requires permission to get your Id.'));
      return $this->redirect('user.login');
    }
    // Gets user's name from the Zalo profile.
    if (!$name = $this->zaloManager->getName($zalo_profile)) {
      $this->messenger->addError($this->t('Zalo login failed. This site requires permission to get your Name.'));
      return $this->redirect('user.login');
    }

    // Zalo API dont have user email.
    // Just Id and Name return from their API.
    // Ex: ["id": "2342343242342", "name": "Lap Pham"].
    // So temporary use the id to authenticate.
    // Will find another solution from other expert developer.
    // Save zalo profile as a json string to extra data.
    return $this->userAuthenticator->authenticateUser($name, NULL, $id, $this->zaloManager->getAccessToken(), FALSE, Json::encode($zalo_profile));
  }

}
