<?php

namespace Drupal\social_auth_zalo;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class ZaloAuthPersistentDataHandler.
 *
 * @see Drupal\social_auth_zalo\ZaloAuthPersistentDataHandler
 * I refer the class about then decide to create this class.
 * I think the Zalo SDK is same same with Zalo SDK so copy the class.
 *
 * @package Drupal\social_auth_zalo
 */
class ZaloAuthPersistentDataHandler {
  protected $session;
  protected $sessionPrefix = 'social_auth_zalo_';

  /**
   * Constructor.
   *
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
   *   Used for reading data from and writing data to session.
   */
  public function __construct(SessionInterface $session) {
    $this->session = $session;
  }

  /**
   * {@inheritdoc}
   */
  public function get($key) {
    return $this->session->get($this->getSessionPrefix() . $key);
  }

  /**
   * {@inheritdoc}
   */
  public function set($key, $value) {
    $this->session->set($this->getSessionPrefix() . $key, $value);
  }

  /**
   * Gets the session prefix for the data handler.
   *
   * @return string
   *   The session prefix.
   */
  public function getSessionPrefix() {
    return $this->sessionPrefix;
  }

}
