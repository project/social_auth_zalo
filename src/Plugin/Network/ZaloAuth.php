<?php

namespace Drupal\social_auth_zalo\Plugin\Network;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\social_auth_zalo\ZaloAuthPersistentDataHandler;
use Drupal\social_api\Plugin\NetworkBase;
use Drupal\social_api\SocialApiException;
use Drupal\social_auth_zalo\Settings\ZaloAuthSettings;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Zalo\Zalo;

/**
 * Defines a Network Plugin for Social Auth Zalo.
 *
 * @Network(
 *   id = "social_auth_zalo",
 *   social_network = "Zalo",
 *   type = "social_auth",
 *   handlers = {
 *     "settings": {
 *       "class": "\Drupal\social_auth_zalo\Settings\ZaloAuthSettings",
 *       "config_id": "social_auth_zalo.settings"
 *     }
 *   }
 * )
 */
class ZaloAuth extends NetworkBase implements ZaloAuthInterface {

  /**
   * The Zalo Persistent Data Handler.
   *
   * @var \Drupal\social_auth_zalo\ZaloAuthPersistentDataHandler
   */
  protected $persistentDataHandler;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The url generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('social_auth_zalo.persistent_data_handler'),
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('url_generator.non_bubbling')
    );
  }

  /**
   * ZaloAuth constructor.
   *
   * @param \Drupal\social_auth_zalo\ZaloAuthPersistentDataHandler $persistent_data_handler
   *   The persistent data handler.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory object.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The url generator.
   */
  public function __construct(ZaloAuthPersistentDataHandler $persistent_data_handler,
                              array $configuration,
                              $plugin_id,
                              array $plugin_definition,
                              EntityTypeManagerInterface $entity_type_manager,
                              ConfigFactoryInterface $config_factory,
                              LoggerChannelFactoryInterface $logger_factory,
                              UrlGeneratorInterface $url_generator) {

    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $config_factory);
    $this->persistentDataHandler = $persistent_data_handler;
    $this->loggerFactory = $logger_factory;
    $this->urlGenerator = $url_generator;
  }

  /**
   * Sets the underlying SDK library.
   *
   * @return \Zalo\Zalo
   *   The initialized 3rd party library instance.
   *
   * @throws \Drupal\social_api\SocialApiException
   *   If the SDK library does not exist.
   * @throws \Zalo\Exceptions\ZaloSDKException
   */
  protected function initSdk() {

    $class_name = '\Zalo\Zalo';
    if (!class_exists($class_name)) {
      throw new SocialApiException(sprintf('The PHP SDK for zalo could not be found. Class: %s.', $class_name));
    }
    /* @var \Drupal\social_auth_zalo\Settings\ZaloAuthSettings $settings */
    $settings = $this->settings;
    if ($this->validateConfig($settings)) {
      $calback_url = $this->urlGenerator->generateFromRoute('social_auth_zalo.return_from_zalo', [], ['absolute' => TRUE]);
      $zalo_settings = [
        'app_id' => $settings->getAppId(),
        'app_secret' => $settings->getAppSecret(),
        'callback_url' => $calback_url,
      ];
      return new Zalo($zalo_settings);
    }
    return FALSE;
  }

  /**
   * Checks that module is configured.
   *
   * @param \Drupal\social_auth_zalo\Settings\ZaloAuthSettings $settings
   *   The Facebook auth settings.
   *
   * @return bool
   *   True if module is configured.
   *   False otherwise.
   */
  protected function validateConfig(ZaloAuthSettings $settings) {
    $app_id = $settings->getAppId();
    $app_secret = $settings->getAppSecret();
    $sdk_version = $settings->getSdkVersion();
    if (!$app_id || !$app_secret || !$sdk_version) {
      $this->loggerFactory
        ->get('social_auth_zalo')
        ->error('Define App ID and App Secret on module settings.');
      return FALSE;
    }

    return TRUE;
  }

}
