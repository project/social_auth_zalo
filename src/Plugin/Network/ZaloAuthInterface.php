<?php

namespace Drupal\social_auth_zalo\Plugin\Network;

use Drupal\social_api\Plugin\NetworkInterface;

/**
 * Defines the Zalo Auth interface.
 */
interface ZaloAuthInterface extends NetworkInterface {}
