<?php

namespace Drupal\social_auth_zalo;

use Drupal\Core\Config\ConfigFactory;
use Drupal\social_auth\AuthManager\OAuth2Manager;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Zalo\Exceptions\ZaloResponseException;
use Zalo\Exceptions\ZaloSDKException;
use Zalo\ZaloEndPoint;

/**
 * Contains all Simple FB Connect logic that is related to Zalo interaction.
 */
class ZaloAuthManager extends OAuth2Manager {

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The url generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * The Zalo persistent data handler.
   *
   * @var \Drupal\social_auth_zalo\ZaloAuthPersistentDataHandler
   */
  protected $persistentDataHandler;

  /**
   * The Zalo client object.
   *
   * @var \Zalo\Zalo
   */
  protected $client;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Implementer settings.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Used for logging errors.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   Used for dispatching events to other modules.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Used for accessing Drupal user picture preferences.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   Used for generating absoulute URLs.
   * @param \Drupal\social_auth_zalo\ZaloAuthPersistentDataHandler $persistent_data_handler
   *   Used for reading data from and writing data to session.
   */
  public function __construct(ConfigFactory $configFactory, LoggerChannelFactoryInterface $logger_factory, EventDispatcherInterface $event_dispatcher, EntityFieldManagerInterface $entity_field_manager, UrlGeneratorInterface $url_generator, ZaloAuthPersistentDataHandler $persistent_data_handler) {
    parent::__construct($configFactory->get('social_auth_zalo.settings'), $logger_factory);
    $this->loggerFactory         = $logger_factory;
    $this->eventDispatcher       = $event_dispatcher;
    $this->entityFieldManager    = $entity_field_manager;
    $this->urlGenerator          = $url_generator;
    $this->persistentDataHandler = $persistent_data_handler;
  }

  /**
   * Returns the Zalo login URL where user will be redirected.
   *
   * @return string
   *   Absolute Zalo login URL where user will be redirected
   */
  public function getZaloLoginUrl() {
    $helper = $this->client->getRedirectLoginHelper();
    // Define the URL where Zalo should return the user.
    $calback_url = $this->urlGenerator->generateFromRoute('social_auth_zalo.return_from_zalo', [], ['absolute' => TRUE]);

    return $helper->getLoginUrl($calback_url);
  }

  /**
   * Get access token by Zalo client.
   *
   * @return mixed|\Zalo\Authentication\AccessToken|null
   *   The access token.
   */
  public function getAccessToken() {
    if (!$this->accessToken) {
      $helper = $this->client->getRedirectLoginHelper();
      $calback_url = $this->urlGenerator->generateFromRoute('social_auth_zalo.return_from_zalo', [], ['absolute' => TRUE]);
      try {
        $access_token = $helper->getAccessToken($calback_url);
      }
      catch (ZaloResponseException $ex) {
        $this->loggerFactory
          ->get('social_auth_zalo')
          ->error('Could not get Zalo access token. ZaloResponseException: @message', ['@message' => json_encode($ex->getMessage())]);
        return NULL;
      }
      catch (ZaloSDKException $ex) {
        // Validation failed or other local issues.
        $this->loggerFactory
          ->get('social_auth_zalo')
          ->error('Could not get Zalo access token. Exception: @message', ['@message' => ($ex->getMessage())]);
        return NULL;
      }

      if (isset($access_token)) {
        $this->accessToken = $access_token;
      }
      else {
        $this->loggerFactory
          ->get('social_auth_zalo')
          ->error('Could not get Zalo access token. UNKNOWN ERROR.');
        return NULL;
      }
    }

    return $this->accessToken;
  }

  /**
   * Authenticates the users by using the access token.
   *
   * @return $this
   *   The current object.
   */
  public function authenticate() {
    $this->client->setDefaultAccessToken($this->getAccessToken());
    return $this;
  }

  /**
   * Get user info from API.
   *
   * @param string $fields
   *   The fields name to get.
   *
   * @return bool|array
   *   Array decode from json object.
   */
  public function getUserInfo($fields = 'id,name') {
    try {
      $response = $this->getClient()->get(ZaloEndpoint::API_GRAPH_ME, $this->accessToken, ['fields' => $fields]);
      return $response->getDecodedBody();
    }
    catch (ZaloResponseException $ex) {
      $this->loggerFactory
        ->get('social_auth_zalo')
        ->error('Could not load Zalo user profile: ZaloResponseException: @message', ['@message' => json_encode($ex->getMessage())]);
    }
    catch (ZaloSDKException $ex) {
      $this->loggerFactory
        ->get('social_auth_zalo')
        ->error('Could not load Zalo user profile: ZaloSDKException: @message', ['@message' => ($ex->getMessage())]);
    }

    // Something went wrong.
    return FALSE;
  }

  /**
   * Get name from profile array.
   *
   * @param array $profile
   *   Array return from Zalo API.
   *
   * @return bool|string
   *   False if name is empty.
   */
  public function getId(array $profile) {
    if ($id = $profile['id']) {
      return $id;
    }

    // Email address was not found. Log error and return FALSE.
    $this->loggerFactory
      ->get('social_auth_zalo')
      ->error('No id in Zalo user profile');
    return FALSE;
  }

  /**
   * Get name from profile array.
   *
   * @param array $profile
   *   Array return from Zalo API.
   *
   * @return bool|string
   *   False if name is empty.
   */
  public function getName(array $profile) {
    if ($id = $profile['name']) {
      return $id;
    }

    // Email address was not found. Log error and return FALSE.
    $this->loggerFactory
      ->get('social_auth_zalo')
      ->error('No name in Zalo user profile');
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getState() {
    // TODO: Implement getState() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthorizationUrl() {
    // TODO: Implement getAuthorizationUrl() method.
  }

  /**
   * {@inheritdoc}
   */
  public function requestEndPoint($method, $path, $domain = NULL, array $options = []) {
    // TODO: Implement requestEndPoint() method.
  }

}
