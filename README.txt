CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Social Auth Zalo allows users to register or login
to your Drupal site with their Zalo account.
This module is base on Social Auth, Social API.
I don't have enough experience to invent a new module
so I referred to the Social Auth Facebook to implement this module.
You will see many code from Social Auth Facebook.
Thanks to contributors of Social API, Social Auth and Social Auth Facebook

REQUIREMENTS
------------

This module requires the following modules:

 * Social Auth (2.0-rc1 - https://www.drupal.org/project/social_auth)

 * Social Api (2.0-rc1 - https://www.drupal.org/project/social_api)

 * Zalo SDK for PHP (v2.0.0 - https://github.com/zaloplatform/zalo-php-sdk)

I included 2 modules and SKD to composer.json

INSTALLATION
------------

 * Install module with "composer require drupal/social_auth_zalo". Visit
 https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies
 for further information.

CONFIGURATION
-------------

 * Create your application at: https://developers.zalo.me/

 * Config application_id, application_secret:
   /admin/config/social-api/social-auth/zalo

 * This module adds a path to redirect: /user/login/zalo

MAINTAINERS
-----------

Current maintainers:

 * Lap Pham (phthlaap) - https://www.drupal.org/user/3579545
